from django.test import TestCase
from rest_framework.test import APITestCase
from django.urls import reverse

from .views import ContactList, ContactDetail
from .models import Contact

import json


# Create your tests here.

class ContactModelTests(TestCase):

    def setUp(self):
        # create a contact
        first_contact = Contact.objects.create(
            username="test contact 1", first_name="Josh",
            last_name="Smith", email="joshsmith@local.com",
        )
        first_contact.save()

    def test_contact_details(self):
        first_contact = Contact.objects.get(pk=1)
        expected_name = first_contact.first_name
        expected_username = first_contact.username
        expected_email = first_contact.email

        self.assertEqual(expected_name, 'Josh')
        self.assertEqual(expected_username, 'test contact 1')
        self.assertEqual(expected_email, "joshsmith@local.com")


class ContactListTest(TestCase):

    def test_correct_response(self):
        response_contact_list = self.client.get('/api/v1/')

        self.assertEqual(response_contact_list.status_code, 200)

    def test_no_objects_exist(self):
        objects = Contact.objects.all()
        self.assertEqual(objects.count(), 0)

    def test_create_and_check_two_objects(self):
        testuser1 = Contact.objects.create(username="testuser1", first_name="tester",
                                           last_name="python", email="testuser@python.com")
        testuser2 = Contact.objects.create(username="testuser2", first_name="tester2",
                                           last_name="python2", email="testuser@python2.com")

        # Two objects should now exist

        self.assertEqual(Contact.objects.count(), 2)


class ContactRetrievalTest(TestCase):

    def test_correct_response(self):
        testuser = Contact.objects.create(username='Josh')
        response = self.client.get(f'/api/v1/{testuser.username}/')
        self.assertEqual(response.status_code, 200)


    def test_contact_retrieval_by_username(self):
        testuser = Contact.objects.create(username='UserJosh')
        response = self.client.get(f'/api/v1/{testuser.username}/')
        self.assertEqual(response.json()[0]['username'], 'UserJosh')


