from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from django.urls import reverse
from django.http import response


from .models import Contact
from .serializers import ContactSerializer
# Create your views here.


class ContactList(generics.ListCreateAPIView):

    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    filter_backends = [SearchFilter]
    search_fields = ['username', 'email']


class ContactDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer


class ContactRetrieval(generics.ListAPIView):
    serializer_class = ContactSerializer

    def get_queryset(self, *args, **kwargs):
        """
        Returns a contact by username e.g. api/v1/testuser1/
        instead of default primary key
        """

        username = self.kwargs['username']
        return Contact.objects.filter(username=username)





