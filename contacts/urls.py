from django.urls import path

from .views import ContactList, ContactDetail, ContactRetrieval

urlpatterns = [
    path('', ContactList.as_view(), name='contacts_list'),
    path('<int:pk>/', ContactDetail.as_view(), name='detail'),
    path('<username>/', ContactRetrieval.as_view(), name='GETuser'),

]